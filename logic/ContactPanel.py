import wx

import data
from wxutils import wxchatutils

users = []
contents = data.msgs

class ContactPanel(wx.Panel):
    width, height = 1200, 800
    offsetX, offsetY = 120, 20

    def __init__(self, parent):
        lf = wxchatutils.get_friends()
        super(ContactPanel, self).__init__(parent=parent)
        self.parent = parent
        sp = wx.ScrolledWindow(self, size=(self.width - 20, self.height - 100))
        sp.SetScrollbars(1, 1, self.width, self.height - 20)
        btn = wx.Button(self, label="确定", pos=(self.width // 2, self.height - 100))
        btn.Bind(wx.EVT_BUTTON, handler=self.on_btn_click)
        for index, user in enumerate(lf):
            x = 10 + self.offsetX * index
            y = 10 + self.offsetY * (x // self.width)
            cb1 = wx.CheckBox(sp, label=wxchatutils.get_name(user), pos=(x % self.width, y))
            cb1.obj = user
            cb1.Bind(wx.EVT_CHECKBOX, handler=self.on_check_changed)  # 回调信息

    def on_btn_click(self, e):
        self.parent.tomsg(users)

    def on_check_changed(self, e):
        cb = e.GetEventObject()
        print(cb.GetLabel(), ' is clicked', cb.GetValue())
        print(cb.obj)
        if cb.obj not in users:
            users.append(cb.obj)
        else:
            users.remove(cb.obj)
