import wx


class BaseWindow(wx.Frame):
    width, height = 1200, 800
    offsetX, offsetY = 120, 20
    panels = list()
    index = 0

    def __init__(self, parent=None, title='', size=(1200, 800)):
        super(BaseWindow, self).__init__(parent=parent, title=title, size=size)

    # 添加容器
    def addPanel(self, panel):
        self.panels.append(panel)
        self.index = len(self.panels) - 1
        self.select(self.index)

    # 切换容器
    def select(self, select_index):
        for index, p in enumerate(self.panels):
            if index == select_index:
                p.Show()
            else:
                p.Hide()

    # 容器回滚
    def reback(self):
        if self.index > 0:
            self.index = self.index - 1
            self.select(self.index)
            self.remove()

    #删除容器
    def remove(self):
        if self.index < len(self.panels)-1:
            p = self.panels
            p.pop(len(self.panels)-1)
            self.panels = p
