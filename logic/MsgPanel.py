import wx
import data

contents = data.msgs
app = wx.App()


class MsgPanel(wx.Panel):
    width, height = 1200, 800
    offsetX, offsetY = 120, 20
    default_value = "摘一片玫瑰，加一分思念，带上我的九百九十九个祝福，轻盈落在你的肩头；给你一片会跳舞的阳光，我们一起走在幸福的路上，不管今天还是未来，我永远将你守候！"

    def __init__(self, parent, users):
        super(MsgPanel, self).__init__(parent=parent, size=(self.width, self.height))
        self.parent = parent
        self.users = users
        et = wx.TextCtrl(self, -1,
                         value=self.default_value,
                         size=(self.width // 2, 100),
                         pos=(self.width // 4, 10),
                         style=wx.TE_MULTILINE | wx.TE_RICH2)
        sv2 = wx.ScrolledWindow(self, size=(self.width - 20, self.height - 200), pos=(0, 100))
        sv2.SetScrollbars(1, 1, self.width, self.height)
        for index, item in enumerate(contents):
            rb = wx.RadioButton(sv2, 22, label=item, pos=(10, 10 + 30 * index))
            rb.Bind(wx.EVT_RADIOBUTTON, handler=self.on_selected)
            rb.value = item
        btn = wx.Button(self, label="生成语句", pos=(self.width // 2, self.height - 100))
        btn.Bind(wx.EVT_BUTTON, handler=self.onbtnGenarate)
        btn2 = wx.Button(self, label="返回", pos=(self.width // 3, self.height - 100))
        btn2.Bind(wx.EVT_BUTTON, handler=self.onreback)

        self.et = et
        self.btn = btn

    def onreback(self,e):
        self.parent.reback()

    def on_selected(self, e):
        rb = e.GetEventObject()
        value = rb.value
        self.et.SetValue(value)
        print("selected" + value)

    def onbtnGenarate(self, e):
        self.parent.show_msg_window(self.users, self.et.GetValue())
