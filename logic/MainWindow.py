# -*- coding: UTF-8 -*-

"""
                ' '^ ..^ .' .,^;;+__<,     ''   ..       
         ` .' ;;~i.  ''.        .:ii?l   ..       
         ..:1`     .l+<;``. .   .'   ..:] ..      
         .ll' ''  .^ll:,^^.   '.  ''.  .'{^       
          >' lii]~,''``^,,,l_1}i. ''...  ,]'      
         ,]:1:^^..``^```''',..,`,_((!',l..{:      
          -<+l<<:!,^^^``^^:i.^. '^::i,'+`^1'      
          `:[_{{l,<|^``';;',<^.`_)<<:<,~<;~       
          `:;1<<il:I,'`,''`^`lI!~^...:i}~_1       
         ' 'l>++Ii>::,;',,^^:;>+_]??,..:;?1       
          ..~+  '^'?[+-<ii{j\~<.. .. . .'I)^   .  
          .;l-   lI:<~'.``` >~Ii~^    .  `-i..  ..
           ^;^,,;,\_i^ .  .'l-1+.^ii'   `:II....  
            .    '<^'''`''`^;::,''       
"""
from logic.BaseWindow import BaseWindow
from logic.ContactPanel import ContactPanel
from logic.MsgPanel import MsgPanel
from logic.resultPanel import resultPanel


class MainWindow(BaseWindow):
    def __init__(self):
        super(MainWindow, self).__init__(None, title="小梁v1.0版", size=(self.width, self.height))
        self.addPanel(ContactPanel(self))

    def tomsg(self, users):
        self.addPanel(MsgPanel(self, users=users))

    def show_msg_window(self, users, data):
        self.addPanel(resultPanel(self, users=users, data=data))

