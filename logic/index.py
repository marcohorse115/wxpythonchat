import wx

from logic.MainWindow import MainWindow

def run():
    app = wx.App()
    MainWindow().Show()
    app.MainLoop()

if __name__ == "__main__":
    run()
