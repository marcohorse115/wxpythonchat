import wx

from logic.BaseWindow import BaseWindow
from wxutils import wxchatutils

app = wx.App()


# 用户类
class User:
    user = ''
    msg = ''
    pass


class resultPanel(wx.Panel):
    width, height = 1200, 800
    offsetX, offsetY = 120, 20
    cbs = set()

    def __init__(self, parent, users, data):
        super(resultPanel, self).__init__(parent, size=(self.width, self.height))
        self.parent = parent
        self.users = users
        self.data = data
        sc = wx.ScrolledWindow(self, size=(self.width - 20, self.height - 100))
        sc.SetScrollbars(1, 1, self.height - 100,self.height - 20 )
        for index, user in enumerate(self.users):
            x = 10
            y = 10 + 100 * index
            label = wxchatutils.get_name(user)
            cb1 = wx.CheckBox(sc, label=label, pos=(x % self.width, y))
            et = wx.TextCtrl(sc, -1,
                             value=label + "，" + data,
                             size=(self.width // 2, 80),
                             pos=(200, y),
                             style=wx.TE_MULTILINE | wx.TE_RICH2)
            cb1.Bind(wx.EVT_CHECKBOX, handler=self.on_check_changed)
            cb1.SetValue(True)
            cb1.user = user
            cb1.et = et
            self.cbs.add(cb1)
            btn = wx.Button(self, label="确定发送", pos=(self.width // 3*2, self.height - 100))
            btn2 = wx.Button(self, label="返回", pos=(self.width // 3, self.height - 100))
            btn.Bind(wx.EVT_BUTTON, handler=self.onbtnSend)
            btn2.Bind(wx.EVT_BUTTON, handler=self.onreback)

    # 发送按钮回调
    def onbtnSend(self, e):
        # todo 发送信息
        for cb in self.cbs:
            if cb.et.GetValue():
                print("发送数据给%s,内容是%s" % (wxchatutils.get_name(user=cb.user), cb.et.GetValue()))
                wxchatutils.send_msg(user=cb.user, content=cb.et.GetValue())

    def onreback(self,e):
        self.parent.reback()

    def on_check_changed(self, e):
        cb = e.GetEventObject()
        print(cb.GetLabel(), ' is clicked', cb.GetValue())
        if cb not in self.cbs:
            self.cbs.add(cb)
        else:
            self.cbs.remove(cb)
