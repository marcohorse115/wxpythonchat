# 1. 导入itchat包
# 2. 获取好友信息列表
# 3. 选择发送好友列表
# 4. 确认发送信息页面
# 5. 发送

# 获取好友的朋友圈信息

import itchat
import json

lf = []  # 微信联系人列表管理
ul = []  # 数据结构


class wx_user:
    name = ''


"""
发送信息用户
通过唯一的UserName定位
直接使用user发送
"""


def send_msg(user=None, userName="", content=''):
    if user:
        user.send(content)
    else:
        itchat.send_msg(msg=content, toUserName=userName)


def get_user_from_name(name):
    user = itchat.search_friends(name=name)
    if user:
        return user[0]
    else:
        print("找不到该用户")
        return None


def get_friends():
    itchat.auto_login(True)
    lf = itchat.get_friends(update=True)
    return lf


"""
获取联系人的username
"""


def get_username(user):
    return user.get("UserName", '')


"""
获取用户名称
none_replace 在remark没有数据的时候用nickreplace代替
nick_replace 直接用nick_name代替
"""


def get_name(user, none_replace=True, nick_replace=False):
    flag = "NickName" if nick_replace else "RemarkName"
    remark_name = user.get(flag, '')
    nick_name = user.get("NickName", '')
    if remark_name == None or remark_name == '':
        remark_name = nick_name if none_replace else ''
    return remark_name


"""
格式化发送列表
"""


def format_content(name, content):
    return ("{}," + content).format(name)


if __name__ == "__main__":
    itchat.auto_login(True)
    lf = itchat.get_friends(update=True)
    for user in lf:
        u = wx_user()
        u.remarkname = get_name(user)
        u.user = user
        u.content = format_content(u.remarkname, "你猜")
        ul.append(u)
    for ull in ul:
        print(ull.content)
    send_msg(user=get_user_from_name("小马哥"), content="wocao")
